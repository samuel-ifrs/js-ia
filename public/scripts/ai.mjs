import { Utils } from "./utils.mjs";

export class Node {
  #layer;
  // Apenas recebe a camada da qual ele faz parte
  constructor(layer) {
    this.#layer = layer;
    this.randomize();
  }

  // Randomiza completamente o nó e os vínculos dele com a camada anterior
  randomize() {
    this.setValue(Math.random());
    if (this.#layer.previous) {
      this.weights = Array.from({ length: this.#layer.previous.length }, () =>
        Utils.map(Math.random(), -1, 1, 0, 1)
      );
    }
  }

  // Realiza uma mutação no nó e nos vínculos dele com a camada anterior
  mutate(maxPerc) {
    if (this.weights) {
      for (let i = 0; i < this.weights.length; i++) {
        this.weights[i] += Math.random() * maxPerc - maxPerc / 2;
      }
    }
  }

  // Obtém o valor do nó, podendo calcular
  getValue(calculating = true, usingSigmoid = false) {
    // Se não tiver camada anterior (for nó de input) ou não estiver calculando, só retorna o valor
    if (!this.#layer.previous || !calculating) {
      return this.value;
    }

    // Obtém o valor multiplicando o valor de cada nó da camada anterior pelo peso da ligação com o nó atual e soma
    const value = this.#layer.previous.nodes
      .map((node, index) => node.getValue() * this.weights[index])
      .reduce((a, b) => a + b);

    // Se for usar Sigmoid (output), então aplica ela ao valor, senão só atualiza o valor
    this.setValue(usingSigmoid ? Utils.sigmoid(value) : value);
    return this.value;
  }

  setValue(value) {
    this.value = value;
  }

  // Inicializa os weights se necessário
  updateWeightLength() {
    if (
      this.#layer.previous &&
      this.#layer.previous.nodes.length !== this.weights?.length
    ) {
      this.randomize();
    }
  }
}

export class Layer {
  // Recebe a quantidade de nós e a camada anterior, se tiver
  constructor(length, previous = null) {
    this.nodes = new Array();
    this.setLength(length);
    this.setPrevious(previous);
  }

  // Define a camada anterior e atualiza os pesos dos nós filhos com os nós da camada anterior
  setPrevious(layer) {
    this.previous = layer;
    this.nodes.forEach((n) => n.updateWeightLength());
  }

  // Atualiza o tamanho, adicionando/removendo nós conforme necessário
  setLength(newLength) {
    while (this.nodes.length < newLength) {
      this.nodes.push(new Node(this));
    }

    while (this.nodes.length > newLength) {
      this.nodes.pop();
    }

    this.length = newLength;
  }

  // Chama o randomize dos nós da camada
  randomize() {
    this.nodes.forEach((node) => node.randomize());
  }

  // Chama o mutate dos nós da camada
  mutate(maxPerc) {
    this.nodes.forEach((node) => node.mutate(maxPerc));
  }
}

export class NeuralNetwork {
  #defaultMidLayerSize;
  #inputLayer;
  #outputLayer;

  // Recebe a quantidade de entradas, camadas intermediárias, saídas e o tamanho das camadas intermediárias
  constructor(inputs, midLayerCount, outputs, defaultMidLayerSize = inputs) {
    this.#defaultMidLayerSize = defaultMidLayerSize;

    this.layers = new Array();

    this.setOutputs(outputs);
    this.setInputs(inputs);
    this.#outputLayer.setPrevious(this.#inputLayer);
    this.setMidLayerCount(midLayerCount);
  }

  getDefaultMidLayerSize() {
    return this.#defaultMidLayerSize;
  }

  // Atualiza o tamanho da camada de entrada
  setInputs(newInputs) {
    this.inputs = newInputs;

    if (this.#inputLayer) {
      this.#inputLayer.setLength(newInputs);
      return;
    }

    this.#inputLayer = new Layer(newInputs);
    this.layers.splice(0, 0, this.#inputLayer);
  }

  // Atualiza o tamanho da camada de saída
  setOutputs(newOutputs) {
    this.outputs = newOutputs;

    if (this.#outputLayer) {
      this.#outputLayer.setLength(newOutputs);
      return;
    }

    this.#outputLayer = new Layer(newOutputs);
    this.layers.push(this.#outputLayer);
  }

  // Adiciona uma camada intermediária na penúltima posição e define que a antepenúltima seja a anterior a ela
  addMidLayer(layerSize) {
    const newLayer = new Layer(layerSize, this.layers[this.layers.length - 2]);
    this.layers.splice(this.layers.length - 1, 0, newLayer);
    this.#outputLayer.setPrevious(newLayer);
  }

  // Remove camada intermediária e ajusta a cadeia de "anteriores"
  popMidLayer() {
    const removing = this.layers.splice(this.layers.length - 2, 1)[0];
    this.#outputLayer.setPrevious(removing.previous);
    return removing;
  }

  getMidLayerCount(layerCount = undefined) {
    return (layerCount || this.layers.length) - 2;
  }

  getMidLayers() {
    return this.layers.slice(1, -1);
  }

  // Adiciona/remove camadas intermediárias
  setMidLayerCount(newMidLayerCount) {
    while (newMidLayerCount < this.getMidLayerCount()) {
      this.popMidLayer();
    }

    while (newMidLayerCount > this.getMidLayerCount()) {
      this.addMidLayer(this.#defaultMidLayerSize);
    }
  }

  // Altera o tamanho das camadas intermediárias
  updateMidLayerSize(newMidLayerSize) {
    this.layers
      .filter(
        (layer) => layer !== this.#inputLayer && layer !== this.#outputLayer
      )
      .forEach((layer) => layer.setLength(newMidLayerSize));

    this.#defaultMidLayerSize = newMidLayerSize;
  }

  // Processa entradas
  process(inputs) {
    // Para cada nó da entrada, atualiza o valor com o valor do parâmetro
    inputs.forEach((value, index) => {
      this.#inputLayer.nodes[index]?.setValue(value);
    });

    // Obtém o valor (calculado) dos nós de saída
    return this.#outputLayer.nodes.map((node) => node.getValue(true, true));
  }

  // Chama a randomização para cada camadas
  randomize() {
    this.layers.forEach((layer) => layer.randomize());
  }

  // Chama a muração de cada camada
  mutate(maxPerc) {
    this.layers.forEach((layer) => layer.mutate(maxPerc));
  }

  // Desenha a rede neural no canvas
  draw(canvas) {
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    const initialLayerX = 50;
    const spaceBetween = 200;
    const layerY = canvas.height / 2;
    const radius = 20;

    this.layers.slice(0, -1).forEach((currentLayer, i) => {
      const nextLayer = this.layers[i + 1];
      for (let j = 0; j < currentLayer.length; j++) {
        for (let k = 0; k < nextLayer.length; k++) {
          const curWeight = nextLayer?.nodes[k].weights?.[j];
          if (curWeight) {
            const intensity = Math.abs(Utils.limit(curWeight));
            const curVal = 100 - Math.round(intensity * 50);
            ctx.strokeStyle = `hsl(${
              curWeight < 0 ? "0" : "120"
            },100%,${curVal}%)`;
          } else {
            ctx.strokeStyle = "#000";
          }
          ctx.beginPath();
          ctx.moveTo(
            initialLayerX + i * spaceBetween + radius + 25,
            layerY + (j - (currentLayer.nodes.length - 1) / 2) * 50
          );
          ctx.lineTo(
            initialLayerX + (i + 1) * spaceBetween,
            layerY + (k - (nextLayer.nodes.length - 1) / 2) * 50
          );
          ctx.stroke();
        }
      }
    });

    this.layers.forEach((currentLayer, i) => {
      for (let j = 0; j < currentLayer.nodes.length; j++) {
        ctx.beginPath();
        if (currentLayer === this.#inputLayer) {
          ctx.fillStyle = "#00f";
        } else if (currentLayer === this.#outputLayer) {
          ctx.fillStyle = "#f00";
        } else {
          ctx.fillStyle = "#000";
        }
        ctx.arc(
          initialLayerX + i * spaceBetween + 25,
          layerY + (j - (currentLayer.nodes.length - 1) / 2) * 50,
          radius,
          0,
          Math.PI * 2
        );
        ctx.fill();
        ctx.fillStyle = "#fff";
        ctx.fillText(
          this.layers[i].nodes[j].getValue(false).toFixed(2),
          initialLayerX + i * spaceBetween + 10,
          layerY + (j - (currentLayer.nodes.length - 1) / 2) * 50
        );
      }
    });
  }

  // Carrega as configurações de outra rede neural
  importFrom(another) {
    this.setMidLayerCount(another.getMidLayerCount());
    this.updateMidLayerSize(another.getDefaultMidLayerSize());

    another.layers.forEach((layer, layerIndex) =>
      layer.nodes.forEach((node, nodeIndex) => {
        this.layers[layerIndex].nodes[nodeIndex].weights = node.weights
          ? [...node.weights]
          : null;
      })
    );
  }
}
