export const Utils = {
  limit: (value, min = -1, max = 1) =>
    value < min ? min : value > max ? max : value,
  map: (value, minTarget, maxTarget, minOrigin = -1, maxOrigin = 1) =>
    ((value - minOrigin) / (maxOrigin - minOrigin)) * (maxTarget - minTarget) +
    minTarget,
  sigmoid: (num) => 1 / (1 + Math.exp(-num)),
};
