import { Utils } from "./utils.mjs";

export class Game {
  constructor(canvas) {
    this.canvas = canvas;
    this.players = [];
    this.pipes = [];
    this.playing = false;
    this.frame = 0;
    this.paused = true;
    this.livingElement = document.getElementById("livingPlayers");
    const framesElement = document.getElementById("frames");
    const timer = () => {
      if (!this.paused) {
        framesElement.innerText = this.frame;
        this.process();
      }
      setTimeout(
        timer,
        parseInt(document.getElementById("interval").value) || 20
      );
    };
    timer();
  }

  start() {
    this.paused = true;
    setTimeout(() => {
      this.frame = 0;
      this.playing = true;
      this.players.forEach((player) => player.start());
      this.pipes = [];
      this.paused = false;
    }, 40);
  }

  pause() {
    this.paused = true;
  }

  resume() {
    this.paused = false;
  }

  addPlayer(controller) {
    const player = new Player(
      this.canvas,
      controller,
      2 / 1000,
      30 / 1000,
      300 / 1000
    );
    this.players.push(player);
    return player;
  }

  addPipe() {
    this.pipes.push(
      new Pipe(
        this.canvas,
        this.canvas.width,
        Utils.map(Math.random(), 50, this.canvas.height - 50, 0, 1),
        30,
        Utils.map(Math.random(), 30, 100, 0, 1)
      )
    );
  }

  removePipe(pipe) {
    this.pipes.splice(
      this.pipes.findIndex((p) => p === pipe),
      1
    );
  }

  draw() {
    const ctx = this.canvas.getContext("2d");
    ctx.fillStyle = "#fff";
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    ctx.fillStyle = "#000";

    this.players.forEach((player) => player.draw(this.canvas));
    this.pipes.forEach((pipe) => pipe.draw(this.canvas));
  }

  process() {
    if (!this.playing) {
      return;
    }
    if (this.frame % 70 === 0) {
      this.addPipe();
    }
    this.frame++;
    this.players.forEach((player) => player.process(this));
    this.pipes.forEach((pipe) => pipe.process(this));
    const livingPlayers = this.players.filter((player) => !player.dead);
    this.livingElement.innerText = livingPlayers.length;
    if (!livingPlayers.length) {
      this.allDead();
    }
  }

  allDead() {
    if (this.doAllDead) {
      this.doAllDead();
    }
    this.pause();
  }
}

class Pipe {
  constructor(canvas, x, y, w, h) {
    this.canvas = canvas;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  process(game) {
    this.x -= 5;
    if (this.x < -this.w) {
      game.removePipe(this);
    }
  }

  draw() {
    const ctx = this.canvas.getContext("2d");
    ctx.fillStyle = "#f00";
    ctx.fillRect(this.x, 0, this.w, this.y - this.h);
    ctx.fillRect(
      this.x,
      this.y + this.h,
      this.w,
      this.canvas.height - this.y - this.h
    );
    ctx.fillStyle = "#000";
  }
}

class Player {
  constructor(canvas, controller, ay, jumpForce, terminalVelocity) {
    this.canvas = canvas;
    this.controller = controller;
    this.start();
    this.ay = ay;
    this.jumpForce = jumpForce;
    this.terminalVelocity = terminalVelocity;
    this.dead = true;
  }

  start() {
    this.pts = 0;
    this.y = 0;
    this.w = 10;
    this.x = this.w;
    this.vy = 0;
    this.controller.shouldJump = false;
    this.dead = false;
  }

  jump() {
    if (this.dead) {
      return;
    }
    this.vy = -this.jumpForce;
    this.pts--;
  }

  process(game) {
    if (this.dead) {
      return;
    }
    if (this.controller.process) {
      this.controller.process(this);
    }
    this.pts++;
    if (this.controller.shouldJump) {
      this.controller.shouldJump = false;
      this.jump();
    }
    this.y = this.y + this.vy;
    if (this.shouldDie(game)) {
      this.dead = true;
      return;
    }
    this.vy = Utils.limit(
      this.vy + this.ay,
      -this.terminalVelocity,
      this.terminalVelocity
    );
  }

  getRealY() {
    return Utils.map(this.y, this.w, this.canvas.height - this.w) - this.w;
  }

  draw() {
    if (this.dead) {
      return;
    }
    const ctx = this.canvas.getContext("2d");
    ctx.fillStyle = this.controller.style || "#000";
    ctx.strokeStyle = "#7f7f7f";
    ctx.fillRect(this.x, this.getRealY(), this.w, this.w);
    ctx.strokeRect(this.x, this.getRealY(), this.w, this.w);
  }

  shouldDie(game) {
    if (this.y < -1 || this.y > 1) {
      return true;
    }
    return game.pipes.some((pipe) => {
      if (this.x + this.w < pipe.x || this.x > pipe.x + pipe.w) {
        return false;
      }
      if (pipe.y - pipe.h > this.getRealY() + this.w) {
        return true;
      }
      if (pipe.y + pipe.h < this.getRealY()) {
        return true;
      }
      return false;
    });
  }
}
