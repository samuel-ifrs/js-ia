import { NeuralNetwork } from "./scripts/ai.mjs";
import { Game } from "./scripts/game.mjs";

window.onload = () => {
  // Cria uma instância do jogo
  const game = new Game(document.getElementById("gameCanvas"));

  // Configura a interface
  document.getElementById("btnReset").onclick = () => game.start();
  document.getElementById("btnResume").onclick = () => game.resume();
  document.getElementById("btnPause").onclick = () => game.pause();

  const intLayerCount = document.getElementById("intLayerCount");
  const intLayerSize = document.getElementById("intLayerSize");
  const aiCount = document.getElementById("aiCount");

  const doChangeNumericInput =
    (shouldRecreate) =>
    ({ target }) => {
      const minValue = parseFloat(target.min);
      const maxValue = parseFloat(target.max);
      if (parseFloat(target.value) === NaN) {
        target.value = minValue || 0;
      }
      if (minValue !== NaN) {
        if (target.value < minValue) {
          target.value = minValue;
        }
      }
      if (maxValue !== NaN) {
        if (target.value > maxValue) {
          target.value = maxValue;
        }
      }
      if (shouldRecreate) {
        createAi();
      }
    };
  intLayerCount.onchange = doChangeNumericInput(true);
  intLayerSize.onchange = doChangeNumericInput(true);
  aiCount.onchange = doChangeNumericInput(true);
  document.getElementById("mutation").onchange = doChangeNumericInput(false);
  document.getElementById("interval").onchange = doChangeNumericInput(false);

  // Inicializações
  let bestPlayer = null;
  let bestPlayerPts = 0;
  let bestGeneration = 1;
  let generation = 1;

  // Desenha o jogo
  const draw = () => {
    game.draw();
    requestAnimationFrame(draw);
  };
  draw();

  // Função para obter os inputs da IA do jogador
  const getInputs = (player) => {
    const nextPipe = game.pipes[0];
    return [
      player.getRealY(),
      player.vy,
      nextPipe?.x || game.canvas.width,
      nextPipe?.y || game.canvas.height / 2,
      nextPipe?.h || 10,
    ];
  };

  function createAi() {
    generation = 1;
    bestPlayerPts = 0;
    bestGeneration = 0;
    document.getElementById("generation").innerText = "1";
    document.getElementById("bestGeneration").innerText = "";
    document.getElementById("maxPtsEver").innerText = "0";
    document.getElementById("maxPtsLast").innerText = "0";

    const intLayerCountValue = intLayerCount.value;
    const intLayerSizeValue = intLayerSize.value;

    const didPause = !game.paused && game.playing;
    if (didPause) {
      game.pause();
      game.players
        .filter((player) => player.controller.ai)
        .forEach((player) =>
          game.players.splice(game.players.indexOf(player), 1)
        );
    }

    // Criar os jogadores de IA
    for (let i = 0; i < aiCount.value; i++) {
      // Cria uma nova instância de rede neural
      // NeuralNetwork (
      //    inputs              = Quantas entradas a rede neural terá,
      //    midLayerCount       = Quantas camadas intermediárias a rede neural terá,
      //    outputs             = Quantas saídas serão calculadas,
      //    defaultMidLayerSize = O tamanho das camadas intermediárias
      //)
      const ai = new NeuralNetwork(5, intLayerCountValue, 1, intLayerSizeValue);
      // Cria um controlador para o jogador
      const controller = {
        ai,
        // Uma cor aleatória pra ficar bonitinho
        style: `hsl(${Math.trunc(Math.random() * 360)}, 100%, 50%)`,
        // Função que será executada em cada frame
        process: (player) => {
          // Obtém os inputs do jogador
          const inputs = getInputs(player);
          // Obtém o valor calculado da primeira (única) saída da rede neural (entre 0 e 1)
          const signal = player.controller.ai.process(inputs)[0];
          // Se for maior que 0.5, pula
          if (signal > 0.5) {
            player.controller.shouldJump = true;
          }
        },
      };

      // Cria o jogador usando o controlador e define ele como bestPlayer
      bestPlayer = game.addPlayer(controller);
    }

    if (didPause) {
      game.start();
    }
  }

  createAi();

  // Desenhar a melhor rede neural atual
  setInterval(() => {
    const bestAiPlayer =
      bestPlayer && !bestPlayer.dead
        ? bestPlayer
        : game.players
            .filter((player) => player.controller?.ai)
            .reduce((curBest, player) =>
              curBest?.pts > player.pts ? curBest : player
            );
    bestAiPlayer.controller.ai.draw(
      document.getElementById("neuralNetworkCanvas")
    );
  }, 100);

  // Quando todos os jogadores morrerem
  game.doAllDead = () => {
    // Obtém todos os jogadores de IA
    const aiPlayers = game.players.filter((player) => player.controller?.ai);
    // Ordena eles do melhor para o pior
    aiPlayers.sort((a, b) => b.pts - a.pts);

    // Verifica se precisa atualizar o melhor jogador de todos
    if (bestPlayerPts < aiPlayers[0].pts) {
      bestPlayer = aiPlayers[0];
      bestPlayerPts = aiPlayers[0].pts;
      bestGeneration = generation;
    }

    // O jogador que já foi o melhor de todos não será modificado
    const mutatingPlayers = aiPlayers.filter((p) => p !== bestPlayer);

    // Obtém o percentual de mutação da interface
    const mutationValue =
      parseFloat(document.getElementById("mutation").value) || 0.1;

    // 1/3 dos melhores sofrem pequenas mutações para melhorar, menos os dois melhores dessa
    mutatingPlayers
      .slice(2, mutatingPlayers.length / 3 + 1)
      .forEach((player) => player.controller.ai.mutate(mutationValue));

    // o 1/3 intermediário vai ser randomizado buscando novas possibilidades
    mutatingPlayers
      .slice(mutatingPlayers.length / 3 + 1, (2 * mutatingPlayers.length) / 3)
      .forEach((player) => player.controller.ai.randomize());

    // o pior 1/3 vai ser uma cópia do melhor e vai sofrer uma pequena mutação
    mutatingPlayers
      .slice((2 * mutatingPlayers.length) / 3)
      .forEach((player) => {
        player.controller.ai.importFrom(bestPlayer.controller.ai);
        player.controller.ai.mutate(mutationValue);
      });

    // Atualiza as estatísticas
    document.getElementById("generation").innerText = ++generation;
    document.getElementById("bestGeneration").innerText = bestGeneration;
    document.getElementById("maxPtsEver").innerText = bestPlayerPts;
    document.getElementById("maxPtsLast").innerText = aiPlayers[0].pts;
    // Inicia o jogo novamente
    game.start();
  };

  // Cria o jogador controlado pelo clique
  const myPlayer = game.addPlayer({
    style: "rgb(0,0,0)",
  });
  game.canvas.onclick = () => (myPlayer.controller.shouldJump = true);

  // Inicia o jogo
  game.start();
};
